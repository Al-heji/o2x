import sys
import time
#import gpio
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QApplication , QWidget ,QFrame, QPushButton , QMessageBox,QProgressBar , QLabel, QGraphicsDropShadowEffect
from PyQt5.QtGui import QIcon,QPixmap
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QThread, pyqtSignal
class ThreadClass(QThread):
    change_value = pyqtSignal(float)
    def run(self):
        val = 10.5
        while 1:
            val+=0.5
            #val = gpio.getValues()
            #progress.setValue(val)
            time.sleep(0.5)
            #self.change_value.emit(val)
            self.change_value.emit(val)
            if val > 26 :
                val = 15.4

class App(QWidget):
    
    def __init__(self):
        super().__init__()
        self.title = 'O2x'
        self.left= 0
        self.top = 0
        self.width = 480
        self.height =320
        self.startLoop()
        self.initUI()
    def stting_open(self):
        buttonReply = QMessageBox.question(self, 'O2x message', "Do you like PyQt5?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            print('Yes clicked.')
            self.frame.hide()
        else:
            print('No clicked.')
            self.frame.show()

    def startLoop(self):
        self.threadclass = ThreadClass()
        self.threadclass.change_value.connect(self.setSignalValue)
        self.threadclass.start()
    def initUI(self):
        
        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon('./ox.png'))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setStyleSheet("background-color:#474749")
        #frame2
        self.frame2 =QFrame(self)
        self.frame2.setFrameStyle(QFrame.Panel | QFrame.Raised)
        self.frame2.setStyleSheet("background-color:brown")
        self.frame2.setGeometry(self.left,self.top,360,self.height)
        #frame1
        self.frame =QFrame(self)
        self.frame.setFrameStyle(QFrame.Panel | QFrame.Raised)
        self.frame.setStyleSheet("background-color:#474749")
        self.frame.setGeometry(self.left,self.top,360,self.height)
        
        #button code ---------------------
        #button1 = QPushButton('',self)
        button1 = HoverButton(self)
        button1.setToolTip('')
        button1.setIcon(QIcon("./logo1.png"))
        button1.setStyleSheet("background-color:#f6f6f6")
        button1.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        button1.setIconSize(QtCore.QSize(105,105))
        button1.move(360,3)
        button1.resize(120,105)
        button1.clicked.connect(self.stting_open)
        #button2 = QPushButton('',self)
        button2 =  HoverButton(self)
        button2.setToolTip('setting')
        button2.setIcon(QIcon("./settingblue.png"))
        button2.setStyleSheet("background-color:#f6f6f6")
        button2.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        button2.setIconSize(QtCore.QSize(105,105))
        button2.move(360,110)
        button2.resize(120,105)
        #button3 = QPushButton('',self)
        button3 = HoverButton(self)
        button3.setIcon(QIcon("./maintenablue.png"))
        button3.setStyleSheet("background-color:#f6f6f6")
        button3.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        button3.setIconSize(QtCore.QSize(105,105))
        button3.setToolTip('Maintenance')
        button3.move(360,216)
        button3.resize(120,105)
        # progressBar
        self.progress = QProgressBar(self.frame)
        self.progress.setGeometry(80,200,250,20)
        # label
        self.label =QLabel('21',self.frame)
        self.label.setGeometry(QtCore.QRect(45,108,320,180))
        newfont = QtGui.QFont("PibotoLt",48,QtGui.QFont.Bold)
        self.label.setStyleSheet('color: #f6f6f6')
        self.label.setFont(newfont)
        label2 =QLabel('%',self.frame)
        label2.setGeometry(QtCore.QRect(260,105,100,100))
        label2.setStyleSheet("background-color:transparent;")
        newfont2 = QtGui.QFont("PibotoLt",27,QtGui.QFont.Bold)
        label2.setFont(newfont2)
        # Create widget
        self.label3 = QLabel(self)
        pixmap = QPixmap('prograssbar.png')
        self.label3.setPixmap(pixmap)
        self.label3.setGeometry(QtCore.QRect(0,90,220,180))
        self.label3.setStyleSheet("background-color:transparent;")
        self.label4 = QLabel(self)
        pixmap2 = QPixmap('prograssbarup.png')
        self.label4.setPixmap(pixmap2)
        self.label4.setGeometry(QtCore.QRect(0,89,220,180))
        self.label4.setStyleSheet("background-color:transparent;")
        self.label5 = QLabel(self)
        pixmap3 = QPixmap('prograssbarRead.png')
        self.label5.setPixmap(pixmap3)
        self.label5.setGeometry(QtCore.QRect(0,89,220,180))
        self.label5.setStyleSheet("background-color:transparent;")
        self.label6 = QLabel(self)
        pixmap4 = QPixmap('sc1.png')
        self.label6.setPixmap(pixmap4)
        self.label6.setGeometry(QtCore.QRect(17,46,220,180))
        self.label6.setStyleSheet("background-color:transparent;")
        self.label7 = QLabel(self)
        pixmap5 = QPixmap('sc2.png')
        self.label7.setPixmap(pixmap5)
        self.label7.setGeometry(QtCore.QRect(33,32,220,180))
        self.label7.setStyleSheet("background-color:transparent;")
        self.label8 = QLabel(self)
        pixmap6 = QPixmap('sc3.png')
        self.label8.setPixmap(pixmap6)
        self.label8.setGeometry(QtCore.QRect(47,25,220,180))
        self.label8.setStyleSheet("background-color:transparent;")
        self.label9 = QLabel(self)
        pixmap7 = QPixmap('sc4.png')
        self.label9.setPixmap(pixmap7)
        self.label9.setGeometry(QtCore.QRect(68,21,220,180))
        self.label9.setStyleSheet("background-color:transparent;")
        self.label10 = QLabel(self)
        pixmap8 = QPixmap('sc5.png')
        self.label10.setPixmap(pixmap8)
        self.label10.setGeometry(QtCore.QRect(90,25,220,180))
        self.label10.setStyleSheet("background-color:transparent;")
        self.label11 = QLabel(self)
        pixmap9 = QPixmap('sc6.png')
        self.label11.setPixmap(pixmap9)
        self.label11.setGeometry(QtCore.QRect(111.8,35.9,220,180))
        self.label11.setStyleSheet("background-color:transparent;")
        self.label12 = QLabel(self)
        pixmap10 = QPixmap('sc7.png')
        self.label12.setPixmap(pixmap10)
        self.label12.setGeometry(QtCore.QRect(127,48.1,220,180))
        self.label12.setStyleSheet("background-color:transparent;")
        
        
        self.show()
        
    def setSignalValue(self,val):
        print(round(val,1))
        self.progress.setValue(val)
        
        if val <=19:
            self.label6.show()
            self.label7.hide()
            self.label8.hide()
            self.label9.hide()
            self.label10.hide()
            self.label11.hide()
            self.label12.hide()
        if val >19 and val <19.5:
            self.label6.show()
            self.label7.show()
            self.label8.hide()
            self.label9.hide()
            self.label10.hide()
            self.label11.hide()
            self.label12.hide()
        if val >=20 and val <20.2:
            self.label6.show()
            self.label7.show()
            self.label8.show()
            self.label9.hide()
            self.label10.hide()
            self.label11.hide()
            self.label12.hide()
        if val >=20.2 and val <20.5:
            self.label6.show()
            self.label7.show()
            self.label8.show()
            self.label9.show()
            self.label10.hide()
            self.label11.hide()
            self.label12.hide()
        if val >=20.5 and val <20.9:
            self.label6.show()
            self.label7.show()
            self.label8.show()
            self.label9.show()
            self.label10.show()
            self.label11.hide()
            self.label12.hide()
        if val >=20.9 and val <24 :
            self.label6.show()
            self.label7.show()
            self.label8.show()
            self.label9.show()
            self.label10.show()
            self.label11.show()
            self.label12.hide()
        if val >=24 :
            self.label6.show()
            self.label7.show()
            self.label8.show()
            self.label9.show()
            self.label10.show()
            self.label11.show()
            self.label12.show()
        
        
        val_str = str(round(val,1))
        self.label.setText(val_str + "%");
#--------------------------------
class HoverButton(QPushButton,QGraphicsDropShadowEffect):

    def __init__(self, parent=None):
        super(HoverButton, self).__init__(parent)
        self.setMouseTracking(True)
    def enterEvent(self,event):
        print("Enter")
        self.setStyleSheet("background-color:#b8c6db; background-image: linear-gradient(315deg, #b8c6db 0%, #f5f7fa 74%);")

    def leaveEvent(self,event):
        self.setStyleSheet("background-color:#f6f6f6")
        print("Leave")

if __name__ == '__main__' :
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
    
